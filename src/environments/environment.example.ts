// This an example environment file.
// Copy this to environment.ts for development or environment.prod.ts for production

export const environment = {
  production: false,
  lockerApi: 'http://localhost:8000/locker/',
  proxyApi: 'http://localhost:8000/proxy/',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/dist/zone-error'; // Included with Angular CLI.
