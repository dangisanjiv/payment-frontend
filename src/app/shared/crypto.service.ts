import { Injectable } from '@angular/core';

// shim for tweetnacl-sealedbox-js
declare var sealedBox: any; // todo: create TS definition?

@Injectable({
  providedIn: 'root'
})
export class CryptoService {

  constructor() {
  }


  binaryToString(bin) {
    const CHUNK_SZ = 0x8000;
    const c = [];
    for (let i = 0; i < bin.length; i += CHUNK_SZ) {
      c.push(String.fromCharCode.apply(null, bin.subarray(i, i + CHUNK_SZ)));
    }
    return c.join('');
  }

  stringToBinary(str) {
    const bin = new Uint8Array(new ArrayBuffer(str.length));
    for (let i = 0; i < str.length; i++) {
      bin[i] = str.charCodeAt(i);
    }
    return bin;
  }

  base64ToBinary(base64) {
    return this.stringToBinary(window.atob(base64));
  }

  encrypt(data: string, publicKey: string): string {
    // convert string data to binary i.e. Uint8Array
    const binData = this.stringToBinary(data);
    const binPublicKey = this.base64ToBinary(publicKey);

    // encrypt data
    const binCipher = sealedBox.seal(binData, binPublicKey);

    // encode encrypted data to base64 and return
    return window.btoa(this.binaryToString(binCipher));
  }
}
