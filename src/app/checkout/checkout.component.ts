import { Component, OnInit } from '@angular/core';
import { PaymentService } from './payment.service';
import { MatSnackBar } from '@angular/material';
import { CryptoService } from '../shared/crypto.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {

  payment: any = {
    amount: null,
    remarks: null,
    token: null
  };

  card: any = {
    name: null,
    number: null,
    expiry: null,
    cvv: null,
  };

  submitting = false;
  paymentSuccess = false;

  constructor(
    private paymentService: PaymentService,
    private snackBar: MatSnackBar,
    private crypto: CryptoService
  ) {

  }

  ngOnInit() {
  }

  showError() {
    this.snackBar.open('Error processing payment', '', {
      duration: 10000
    });
  }

  submit(form) {
    if (form.invalid) {
      return;
    }

    this.submitting = true;

    // fetch public key
    this.paymentService.fetchPublicKey().subscribe((keyResp: any) => {
      // encrypt card data
      const cardCipher = this.crypto.encrypt(JSON.stringify(this.card), keyResp.public_key);
      // send encrypted card data to locker
      this.paymentService.sendCardToLocker({
        payload: cardCipher
      }).subscribe((tokResp: any) => {
        console.log('Card stored to locker');
        // submit payment to proxy
        this.payment.token = tokResp.token;
        this.paymentService.submitPayment(this.payment).subscribe((pmtResp) => {
          // payment success
          this.paymentSuccess = true;
          console.log('Payment success');
          this.snackBar.open('Payment success', '', {
            duration: 10000
          });
        }, (error) => {
          this.submitting = false;
          console.error('Failed to submit payment', error);
          this.showError();
        });
      }, (error) => {
        this.submitting = false;
        console.error('Failed store card to locker', error);
        this.showError();
      });
    }, (error) => {
      this.submitting = false;
      console.error('Failed to fetch public key', error);
      this.showError();
    });
  }

  resetView() {
    this.payment = {
      amount: null,
      remarks: null,
      token: null
    };

    this.card = {
      name: null,
      number: null,
      expiry: null,
      cvv: null,
    };

    this.submitting = false;
    this.paymentSuccess = false;
  }
}
