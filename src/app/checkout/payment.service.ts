import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class PaymentService {


  constructor(private http: HttpClient) {
  }

  fetchPublicKey() {
    return this.http.get(environment.lockerApi + 'public-key/');
  }

  sendCardToLocker(cardPayload: any) {
    return this.http.post(environment.lockerApi + 'store-card/', cardPayload, httpOptions);
  }

  submitPayment(payment: any) {
    return this.http.post(environment.proxyApi + 'submit-payment/', payment, httpOptions);
  }

}
